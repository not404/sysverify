name   = sysverify
target = sysVerify
version = 0.2.4
iteration = 0

maintainer = Will Van Devender <gh@ibvd.net>
description = sysVerify is a simple and lightweight test runner to help with validating server configurations.
platform=amd64

tmp_dir=/tmp/$(name)_pkg

all: clean $(name)

clean:
	rm -f $(target)
	rm -rf $(tmp_dir)

$(name):
	cargo build --release
	cp ./target/release/$(name) $(target)

package:
	mkdir -p $(tmp_dir)/usr/local/bin
	mkdir -p $(tmp_dir)/etc/sysverify.d/
	cp ./target/release/$(name) $(tmp_dir)/usr/local/bin/$(target)
	cp -a ./examples/*.yml $(tmp_dir)/etc/sysverify.d/

	fpm -s dir -t deb -n $(name) -v $(version) \
		--iteration $(iteration)           \
		--directories /etc/sysverify.d     \
		--maintainer "$(maintainer)"       \
		--vendor     "$(maintainer)"       \
		--description "$(description)"     \
		-C $(tmp_dir)

	rm -rf $(tmp_dir)

upload:
	aws s3 cp ./sysVerify  s3://sysverify
	aws s3 cp ./sysverify_$(version)-$(iteration)_$(platform).deb  s3://sysverify

install: $(name)
	sudo cp $(target) ~/bin

uninstall:
	rm ~/bin/$(target)

init:
	cargo install
	aws s3 mb s3://sysverify

test:
	cargo test

# If the first argument is "run"...
ifeq (run,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

run:
	$(interpreter) $(main) $(RUN_ARGS)
