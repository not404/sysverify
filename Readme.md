sysVerify
=========


Description:
------------

sysVerify is a simple and lightweight test runner to help with validating server configurations.  

The tests are organized into yaml files like so:

```yaml
  ---
  name: Example Test Suite
  tests:
    - test: Syslog Active
      command: systemctl is-active syslog

    - test: Syslog Set to start on boot
      command: systemctl is-enabled syslog
```

Anything that can be done in Bash can be executed as the test.


Using sysVerify:
----------------

If something goes wrong on a system, you can use sysVerify to quickly validate that nothing important has changed since the system was built.  Yaml files can be run directly like `sysVerify ./example.yml`, or many yaml files can be grouped together into /etc/sysverify.d/ and run together by just running `sysVerify` with no args.

  ![pic](images/sysVerify01.png)


However, sysVerify really shines at validating a deployment before an image is stamped.  Because the test suite itself returns an error code for how many tests fail, it can be baked into an Ansible play:

```yaml
  # Ansible tasks/main.yml
  - name: Run sysVerify tests
    command: sysVerify /etc/sysverify.d/example-test.yml
    register: sysVerify
    tags:
      - tests 
  
  - name: sysVerify Check
    assert: 
      that:
        - sysVerify.rc == 0
      fail_msg: "Tests showed failures"
    tags:
      - tests
```

*Note*:

Because code reuse in a Config Management system is often done by creating roles & modules and sharing those roles and modules across many different types of systems, it is good practice for each role or module to install its own test into `/etc/sysverify.d/` and run that test at the end of the module's play.  This way each module can be tested independently as it installs.  Then to test the system as a whole, a final run of sysVerify at the completion of the build will re-run all tests.  This validates that no later module changed something deployed by a previous module. 



Installation:
-------------

sysVerify is distributed as a single binary (coded in [Rust](https://www.rust-lang.org/)) prebuilt for linux x86 systems.  

1. [Download Link](https://d336frb9byadlx.cloudfront.net/sysVerify),
2. Create the /etc/sysverify.d directory, 
3. Write some yaml files and go to town. [Example Tests](https://gitlab.com/ibvd/sysverify/-/blob/master/examples/example.yml)

Or if you prefer a debian .deb to make things easier:

1. [Download .deb](https://d336frb9byadlx.cloudfront.net/sysverify_0.2.4-0_amd64.deb),
2. `dpkg -i sysverify_0.2.4-0_amd64.deb`

Or if you prefer to build your own ... (Assuming you have a recent rust environment):

```bash
  git clone ...
  cd sysVerify
  cargo build --release
  sudo cp ./target/release/sysverify /usr/local/bin/sysVerify
```

Background:
-----------

It was found in a large company that considerable effort was spent maintaining automated infrastructure against bit-rot.  Over time base platforms, security polices, network environments or other environmental dependencies would change.  Bit rot would then raise its ugly head, and deploys would begin to fail.  Code that worked fine on RHEL version X, would have bugs when deployed on AWS Linux version Y.  Code that worked fine when landing on an Cloud Vendor A, would fail when the company shifted to Cloud Vendor B.  

Generating artifacts helped avoid the pitfalls of failed deploys.  But this just shifted the problem to maintaining recent versions of all the artifacts which were baked against the latest security policies, OS versions, etc.  

What was needed was to rebuild the artifacts across hundreds of large application environments, and detect when anything was not working correctly.  For this, automated testing was required.  Anything less, and the portfolio of apps would start falling behind on the march of technology and progress. 

Once tests were written, images for long-lived, old-school servers could be tested for correctness in the same manner as used for CI/CD pipelines. And that -- combined with good monitoring -- made it possible to update artifacts in mass, and keep a fleet of systems operationally stable, despite frequent updates in base technology.

If you can move to PAAS or K8S and do not need a lot of servers, good on you.  But if you are in a corporate environment and have a lot of niche applications running on long-lived servers, sysVerify may help you keep those systems up to date.


Comparison w/ InSpec:
---------------------

Although the early versions of sysVerify predate the public release of [Chef's InSpec](https://www.chef.io/products/chef-inspec), sysVerify was never advanced past being a basic test runner.  A simple tool to run Bash snippets was all we needed. And when we tried to switch, it was difficult.  Our engineers really did not warm to Inspec's DSL.  InSpec was just a bit too heavy and slow and verbose.

But if you are starting from scratch, a shop today will likely benefit from a full featured tool like InSpec:

* InSpec is multi-platform, running on both Windows & Linux.  sysVerify is Linux only.
* InSpec tests are much easier to read, as the DSL -- though a pain to write -- reads like english.  sysVerify uses shell, and well, shell is usually easier to write than read.
* InSpec has modules to help with more complex situations which would require many lines of bash.  sysVerify does not.
* InSpec allows for remote testing from a central system.  sysVerify must be installed on each server under test.

However, there are some reasons to favor a smaller tool like sysVerify:

* Lots of tests work much better than no tests. If you find cut & pasting bash into a yaml file makes you write more tests, sysVerify is for you.
* sysVerify is a small, standalone executable.  It installs anywhere in seconds.  A recent `gem install` of InSpec took me > 12 minutes on a 2GB cloud server.  The bloat saddens my heart.
* sysVerify is quick. A hello-world test in InSpec took 3 seconds compared to 1/10th of a second on sysVerify.
* I find the /etc/\*.d/ directory structure both integrate very well with many configuration management systems and promotes test reuse. 
* The format of the output for sysVerify is very compact.  I find it easier to scan the results on large test suites.

In the end what matters is having plenty of tests, and having those tests where they are in easy reach. 


