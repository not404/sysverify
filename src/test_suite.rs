/// TestSuite
/// Common tests get grouped together into a test suite... which ususally would be one
/// YAML file holding all the shell commands we want to execute to validate a particular piece of
/// software or its configuration.
/// The Yaml file will be of the format:
/// ---
/// name: My Service
/// tests:
///   - test: Service installed
///     command: <bash logic to see if the service is installed>
///   - test: Service configured
///     command: <bash logic>


use crate::test::{Test, TestStatus, TestResult};
use colored::*;

// Used to read a yaml file 
//#[allow(unused_imports)]
use std::fs;
use shellexpand::tilde;
use serde::{Deserialize};

/// ResultSet
/// After a TestSuite runs, describes the results
/// Containing the number of tests run and how many failed
#[derive(Debug, PartialEq)]
pub struct ResultSet {
    pub total_tests: usize,
    pub failures: usize,
    pub results: Vec<TestResult>

}

/// TestSuite
#[derive(Debug, PartialEq, Deserialize)]
pub struct TestSuite {
    pub name: String,
    pub tests: Vec<Test>,
}

impl TestSuite {
    /// Run all the test in the suite
    /// Returns the number of failures
    pub fn run(&self) -> ResultSet {
        let mut fail_count: usize = 0;
        let mut results: Vec<TestResult> = Vec::new();

        for test in &self.tests {
            let result = test.run();
            if result.status == TestStatus::FAIL {
                fail_count += 1;
            }
            results.push(result);
        }

        ResultSet{ total_tests: self.tests.len(), failures: fail_count, results: results }
    }

    pub fn print_all(&self, res_set: &ResultSet) {
        let header = format!("Results for {}", self.name).yellow();
        println!("{}", header);

        for res in &res_set.results {
            if res.status == TestStatus::PASS {
                let status = "  PASS".green();
                println!("{}: {}", status, res.name);
            } else {
                let status = "  FAIL".red();
                println!("{}: {}", status, res.name);
            }
        }
    }

    pub fn print_failures(&self, res_set: &ResultSet) {
        let header = format!("Results for {}", self.name).yellow();
        println!("{}", header);

        for res in &res_set.results {
            if res.status == TestStatus::FAIL {
                let status = "  FAIL".red();
                println!("{}: {}", status, res.name);
            }
        }
    }

    /// Create a Test Suite from a YAML string
    fn from_yaml(contents: &str) -> TestSuite {
        let result: TestSuite = match serde_yaml::from_str(contents) {
            Ok(x) => x,
            Err(error) => {
                // panic!("Could not parse yaml: {:?}", error),
                println!("Could not parse yaml {:?}", error);
                std::process::exit(-1);
            },
        };
        result
    }

    /// Create a Test Suite from the contents of a YAML file
    pub fn from_file(path: &str) -> TestSuite {
        let expanded_path = String::from(tilde(&path));
        let contents: String = match fs::read_to_string(expanded_path) {
            Ok(x) => x,
            Err(error) => { 
                println!("Could not open {}: {}", path, error);
                std::process::exit(-1);
            }
        };

        TestSuite::from_yaml(&contents)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn gen_test_suite() -> TestSuite {
        let my_yaml_str = "
---
name: 'Bash Stuff'
tests:
  - test: Bash Comparitor
    command: '[[ 0 -eq 0 ]]'
  - test: Bin True
    command: /bin/true
  - test: Bin False
    command: /bin/false
";
        TestSuite::from_yaml(my_yaml_str)
    }

    #[test]
    fn test_result_set() {
        let ts = gen_test_suite();
        let result_set = ts.run();
        let results = Vec::from([ 
            TestResult{ status: TestStatus::PASS, name: String::from("Bash Comparitor") },
            TestResult{ status: TestStatus::PASS, name: String::from("Bin True") },
            TestResult{ status: TestStatus::FAIL, name: String::from("Bin False") },
        ]);

        let expected = ResultSet{ total_tests: 3, failures: 1, results: results };

        assert_eq!(expected, result_set);
    }

    #[test]
    fn test_from_yaml() {
        let test0 = Test {
            test: String::from("Bash Comparitor"),
            command: String::from("[[ 0 -eq 0 ]]"),
        };

        let test1 = Test {
            test: String::from("Bin True"),
            command: String::from("/bin/true"),
        };

        let test2 = Test {
            test: String::from("Bin False"),
            command: String::from("/bin/false"),
        };

        let expected = TestSuite{ name: String::from("Bash Stuff"), tests: Vec::from([test0, test1, test2]) };

        let suite0 = gen_test_suite();

        assert_eq!(expected, suite0);

    }

    #[test]
    fn test_suite_run() {
        let test0 = Test {
            test: String::from("Bash Comparitor"),
            command: String::from("[[ 0 -eq 0 ]]"),
        };

        let test1 = Test {
            test: String::from("Bin True"),
            command: String::from("/bin/true"),
        };

        let test2 = Test {
            test: String::from("Bin False"),
            command: String::from("/bin/false"),
        };

        let suite = TestSuite { 
                            name: String::from("Burn In"), 
                            tests: Vec::from([ test0, test1, test2 ])

        };

        // One Test should fail in the suite
        assert_eq!(suite.run().failures, 1);
    }
}
