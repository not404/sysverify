use clap::{App, Arg};

pub mod test;
mod test_suite;
use test_suite::{TestSuite, ResultSet};

fn main() {

    /* ****************************** *
     * Process Command Line Arguments *
     * ****************************** */
    let matches = App::new("sysVerify")
            .version("0.2.4")
            .author("iBVD")
            .about("Bash Test Runner")
            .arg(Arg::with_name("FILENAME")
                .index(1)
                .required(false)
                .default_value("/etc/sysverify.d/")
                .takes_value(true)
                .help("filename or directory name of yaml test suite"))
            .arg(
                Arg::with_name("output")
                .short("o")
                .long("output")
                .takes_value(true)
                .default_value("all")
                .possible_values(&["quiet", "all", "fails"]))
            .get_matches();

    /* **************************************** *
     * Iterate over each file and run the tests *
     * **************************************** */

    let mut test_results: Vec<ResultSet> = Vec::new();

    
    let file_name = matches.value_of("FILENAME").unwrap();
    let path = std::path::Path::new(file_name);

    if path.is_file() { 
        
        // We only have one file so just run it
        let res = process_file(path, &matches);
        test_results.push(res);

    } else if path.is_dir() {

        // We have a directory, run tests on files with a .yml extension 
        for entry in path.read_dir().expect("Could not read directory") {
            if let Ok(entry) = entry {
                // Only handle files with a .yml extension
                if "yml" == entry.path().extension().unwrap() {
                    let res = process_file(&entry.path(), &matches);
                    test_results.push(res);
                }
            }
        }
    } else {
        println!("{:?} must be a file or directory", path);
        std::process::exit(-1);
    }
    
    /* ****************************** *
     * Print out the Summary and Exit *
     * ****************************** */

    let mut total_tests: usize = 0;
    let mut failure_count: usize = 0;

    for result in test_results {
        total_tests += result.total_tests;
        failure_count += result.failures;
    }

    println!("{} test(s) failed out of {}", failure_count, total_tests);

    std::process::exit(failure_count as i32);
}


/// Run the tests in the file and optionally print out a result table
fn process_file(path: &std::path::Path, matches: &clap::ArgMatches) -> ResultSet {
    let suite = TestSuite::from_file(path.to_str().unwrap());
    let res = suite.run();

    match matches.value_of("output").unwrap() {
        "all"   => suite.print_all(&res),
        "fails" => suite.print_failures(&res),
        _ => {},
    }

    res
}
