/* ************ *
 * Dependencies *
 * ************ */
use std::process::{Command};
use serde::{Serialize, Deserialize};
// use colored::*;

#[derive(Debug, PartialEq)]
pub enum TestStatus {
    PASS,
    FAIL
}

#[derive(Debug, PartialEq)]
pub struct TestResult {
    pub status: TestStatus,
    pub name: String,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Test {
    pub test: String,
    pub command: String,
}

impl Test {
    pub fn run(&self) -> TestResult {
    let cmd_args = shell_words::split(&self.command).expect("failed to parse command");
    let res = if cfg!(target_os = "windows") {
        Command::new("cmd")
                .arg("/C")
                .args(&cmd_args)
                .output()
                .expect("failed to execute process")
    } else {
        Command::new("/bin/bash")
                .arg("-c")
                .args(&cmd_args)
                .output()
                .expect("failed to execute process")
    };

        /*
        if res.status.success() {
            let status = "  PASS".green();
            println!("{}: {}", status, &self.test);
            TestResult::PASS
        } else {
            let status = "  FAIL".red();
            println!("{}: {}", status, &self.test);
            TestResult::FAIL
        }
        */
        if res.status.success() {
            TestResult{ status: TestStatus::PASS, name: String::from(&self.test) }
        } else {
            TestResult{ status: TestStatus::FAIL, name: String::from(&self.test) }
        }
    }
}

/* ******************* *
 *       Tests         *
 * ******************* */
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_run() {
        let test0 = Test {
            test: String::from("Bash Comparitor"),
            command: String::from("[[ 0 -eq 0 ]]"),
        };

        assert_eq!(test0.run(), TestResult{ name: String::from("Bash Comparitor"), status: TestStatus::PASS});

        let test1 = Test {
            test: String::from("Bin True"),
            command: String::from("/bin/true"),
        };

        assert_eq!(test1.run(), TestResult{ name: String::from("Bin True"), status: TestStatus::PASS});

        let test2 = Test {
            test: String::from("Bin False"),
            command: String::from("/bin/false"),
        };

        assert_eq!(test2.run(), TestResult{ name: String::from("Bin False"), status: TestStatus::FAIL});
    }
}


